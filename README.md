.bash_aliases by Linutux
====================

About
-----
...

Installation
------------
Make sure you don't have a .bash_aliases in your home directory, then
```
wget https://gitlab.com/Linutux/bash-aliases-linutux/raw/master/.bash_aliases -O ~/.bash-aliases
```
and then reopen your shell windows.
